import React from 'react';
import { Text } from '@sitecore-jss/sitecore-jss-react';

import './index.css';

const Michael = (props) => {
  console.log(props);
  return (
    <div>
      <div className="michael_box" style={ {width: `${props.fields.boxWidth}px`, height: `${props.fields.boxHeight}px` }} />
      <Text field={props.fields.label} />
    </div>
  );
}

export default Michael;
