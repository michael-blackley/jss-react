import {render} from '@testing-library/react';
import '@testing-library/jest-dom';
import React from 'react';

import Michael from './index';

test('Render Michael', ()=> {
    const props = {
        fields: {
            label: {
                value: 'Michael test',
            }
        }
    };
    
    const rendered = render(<Michael {...props}/>);
    rendered.debug();

    expect(rendered.getByText('Michael test')).toBeInTheDocument();
});
